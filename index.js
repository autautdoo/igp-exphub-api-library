class ExpHubApi {
    constructor(config) {
        this.language = config.hasOwnProperty('language') ? config.language : 'en'

    }

    result(code, data) {
        return {
            code: code,
            data: data
        }
    }


    getLanguage() {
        console.log("ExperienceHub - Get Language");
        let instance = this;
        return new Promise(function (fulfill, reject) {
            fulfill({
                language: instance.language
            });
            reject(instance.result("get_language_system_error", {reason: 105}));
            /*		let headers = new Headers({
             "Content-Type": "application/json",
             });
             fetch(instance.url+'winners/'+action+'/'+type+toQuery(options), {
             method: 'GET',
             credentials: 'same-origin'
             }).then(function (response){
             response.json().then(function (response) {
             if(!response.hasOwnProperty("info")){
             reject(instance.result("get_winners_system_error"));
             return;
             }
             if(instance.handleErrors(response)){
             reject();
             }else{
             let success = response.info.success;
             let resultCode = response.info.resultCode;
             if(success){
             fulfill(response.data);
             }else{
             switch(resultCode){
             case "system_error":
             reject(instance.result("get_winners_system_error"));
             break;
             default:
             reject(instance.result("get_winners_system_error"));
             break;
             }
             }
             }
             }).catch(function(){
             reject(instance.result("get_winners_system_error"));
             });
             }).catch(function (error) {
             reject(instance.result("get_winners_system_error"));
             });*/
        });

    }

    login(username, password) {
        console.log("ExperienceHub - Login");
        let instance = this;
        return new Promise(function (fulfill, reject) {
            fulfill({
                username: username,
                password: password
            });
            reject(instance.result("login_system_error"));
            /*let headers = new Headers({
             "Content-Type": "application/json",
             });
             fetch(instance.url+'login', {
             method: 'POST',
             body: JSON.stringify({
             username: username,
             password: password
             }),
             credentials: 'same-origin'
             })
             .then(function (response){
             response.json().then(function (response) {
             if(!response.hasOwnProperty("info")){
             reject(instance.result("login_system_error"));
             return;
             }
             if(instance.handleErrors(response)){
             reject();
             }else{
             let success = response.info.success;
             let resultCode = response.info.resultCode;
             if(success){
             instance.syncPlayer();
             fulfill();
             }else{
             switch(resultCode){
             case "missing_password":
             reject(instance.result("login_missing_password"));
             break;
             case "missing_username":
             reject(instance.result("login_missing_username"));
             break;
             case "invalid_username_or_password":
             reject(instance.result("login_invalid_username_or_password"));
             break;
             case "account_blocked":
             reject(instance.result("login_account_blocked"));
             break;
             case "account_frozen":
             reject(instance.result("login_account_frozen"));
             break;
             case "account_pending":
             reject(instance.result("login_account_pending"));
             break;
             case "account_deactivated":
             reject(instance.result("login_account_deactivated"));
             break;
             case "account_unactivated":
             reject(instance.result("login_account_unactivated"));
             break;
             case "system_error":
             reject(instance.result("login_system_error"));
             break;
             default:
             reject(instance.result("login_system_error"));
             break;
             }
             }
             }
             }).catch(function(){
             reject(instance.result("login_system_error"));
             });
             }).catch(function (error) {
             reject(instance.result("login_system_error"));
             });*/
        });
    }

    logout() {
        console.log("ExperienceHub - Logout");
        let instance = this;
        return new Promise(function (fulfill, reject) {
            fulfill(instance.result("logout_success"));
            reject(instance.result("logout_system_error"));
            /*let headers = new Headers({
             "Content-Type": "application/json",
             });
             fetch(instance.url+'logout', {
             method: 'GET',
             credentials: 'same-origin'
             })
             .then(function (response){
             response.json().then(function (response) {
             instance.unsetPlayer();
             instance.setAuth(false);
             fulfill(instance.result("logout_success"));
             }).catch(function(){
             reject(instance.result("logout_system_error"));
             });
             }).catch(function (error) {
             reject(instance.result("logout_system_error"));
             });*/
        });

    }

    getTranslations() {
        return new Promise(function (fulfill, reject) {
            fulfill();
            reject();
            /*fetch(translationsUrl, {
             method: 'GET',
             credentials: 'same-origin'
             })
             .then(function (response){
             response.json().then(function (response) {
             fulfill(response);
             });
             }).catch(function (error) {
             reject();
             });*/
        });
    }

    getAnonSession() {
        console.log("ExperienceHub - Get Anon Session");
        let instance = this;
        return new Promise(function (fulfill, reject) {
            fulfill();
            reject(instance.result("get_anon_session_system_error"));
            /*let headers = new Headers({
             "Content-Type": "application/json",
             });
             fetch(instance.sp_url+'anon', {
             method: 'GET',
             credentials: 'same-origin'
             })
             .then(function (response){
             response.json().then(function (response) {
             if(!response.hasOwnProperty("info")){
             reject(instance.result("get_anon_session_system_error"));
             return;
             }else{
             let success = response.info.success;
             if(success){

             fulfill();
             }else{
             reject(instance.result("get_anon_session_system_error"));
             }
             }
             }).catch(function(){
             reject(instance.result("get_anon_session_system_error"));
             });
             }).catch(function (error) {
             reject(instance.result("get_anon_session_system_error"));
             });*/
        });

    }

    getGames() {
        console.log("ExperienceHub - GET GAMES");
    }

    setCurrencyType(currency) {
        console.log("ExperienceHub - Toggle Currency Type");
        let instance = this;
        return new Promise(function (fulfill, reject) {
            fulfill();
            reject();
            /*let headers = new Headers({
             "Content-Type": "application/json",
             });
             fetch(instance.url+'currencySwitch/'+currency+'?auth='+instance.auth, {
             method: 'GET',
             credentials: 'same-origin'
             })
             .then(function (response){
             response.json().then(function (response) {
             fulfill();
             });
             }).catch(function (error) {
             reject();
             });*/
        });
    }

    getWinners(action, type, options) {
        console.log("ExperienceHub - Get Winners");
        let instance = this;
        return new Promise(function (fulfill, reject) {
            fulfill(response.data);
            reject(instance.result("get_winners_system_error"));
            /*let headers = new Headers({
             "Content-Type": "application/json",
             });
             fetch(instance.url+'winners/'+action+'/'+type+toQuery(options), {
             method: 'GET',
             credentials: 'same-origin'
             }).then(function (response){
             response.json().then(function (response) {
             if(!response.hasOwnProperty("info")){
             reject(instance.result("get_winners_system_error"));
             return;
             }
             if(instance.handleErrors(response)){
             reject();
             }else{
             let success = response.info.success;
             let resultCode = response.info.resultCode;
             if(success){
             fulfill(response.data);
             }else{
             switch(resultCode){
             case "system_error":
             reject(instance.result("get_winners_system_error"));
             break;
             default:
             reject(instance.result("get_winners_system_error"));
             break;
             }
             }
             }
             }).catch(function(){
             reject(instance.result("get_winners_system_error"));
             });
             }).catch(function (error) {
             reject(instance.result("get_winners_system_error"));
             });*/
        });

    }

    getCashierMethods() {
        console.log("ExperienceHub - Get Cashier Methods");
        let instance = this;
        return new Promise(function (fulfill, reject) {
            fulfill({});
            reject(instance.result("get_cashier_methods_system_error"));
            /*let session = ---Cookies.get('xigpsid')---'dummy';
             let headers = new Headers({
             "Content-Type": "application/json",
             "X-igp-session": session
             });
             fetch(--- instance.sp_url+'player/cashier/custom/fields/both' --- instance.url+'customCashier/both', {
             method: 'GET',
             headers: headers,
             credentials: 'same-origin'
             })
             .then(function (response){
             response.json().then(function (response) {
             if(!response.hasOwnProperty("info")){
             reject(instance.result("get_cashier_methods_system_error"));
             return;
             }
             if(instance.handleErrors(response)){
             reject();
             }else{
             let success = response.info.success;
             let resultCode = response.info.resultCode;
             if(success){
             fulfill(response.data);
             }else{
             switch(resultCode){
             default:
             reject(instance.result("get_cashier_methods_system_error"));
             break;
             }
             }
             }
             }).catch(function(){
             reject(instance.result("get_cashier_methods_system_error"));
             });
             }).catch(function (error) {
             reject(instance.result("get_cashier_methods_system_error"));
             });*/
        });

    }

    getBonusWallets() {
        console.log("ExperienceHub - Get Bonus Wallets");
        let instance = this;
        return new Promise(function (fulfill, reject) {
            fulfill({wallets: []});
        });
    }

    getGamesData() {
        let instance = this;
        return new Promise(function (fulfill, reject) {
            fulfill([]);
        });
    }

    submitPayment(processor, type, method, fields) {
        console.log("ExperienceHub - Submit Payment");
        let instance = this;
        return new Promise(function (fulfill, reject) {
            fulfill();
            reject(instance.result("submit_payment_system_error"));
            /*let session = ---Cookies.get('xigpsid')---'dummy';
             let headers = new Headers({
             "Content-Type": "application/json",
             ---"X-igp-session": session---
             });
             queryString="?processor="+processor+"&method="+method+"&type="+type
             switch(processor){
             case "paymentiq":
             queryString += "&txId=\${ptx.id}";
             }
             type = type == "withdraw" ? "withdrawal" : type;
             fetch(---instance.sp_url+'player/cashier/payment/'---instance.url+'submitPayment/'+processor+'/'+type+'/'+method, {
             method: 'POST',
             headers: headers,
             body: JSON.stringify({
             successUrl: instance.paymentOptions.successUrl+queryString,
             failureUrl: instance.paymentOptions.failureUrl+queryString,
             fields: fields,
             }),
             credentials: 'same-origin'
             })
             .then(function (response){
             response.json().then(function (response) {
             if(!response.hasOwnProperty("info")){
             reject(instance.result("submit_payment_system_error"));
             return;
             }
             if(instance.handleErrors(response)){
             reject();
             }else{
             let success = response.info.success;
             let resultCode = response.info.resultCode;
             if(success){
             fulfill(response.data);
             }else{
             switch(resultCode){
             case "payment_provider_error":
             reject(instance.result("submit_payment_payment_provider_error", response.data));
             break;
             case "invalid_input":
             reject(instance.result("submit_payment_invalid_input", response.data));
             break;
             case "payment_processor_error":
             default:
             reject(instance.result("submit_payment_system_error"));
             break;
             }
             }
             }
             }).catch(function(){
             reject(instance.result("submit_payment_system_error"));
             });
             }).catch(function (error) {
             reject(instance.result("submit_payment_system_error"));
             });*/
        });

    }

    getTransactionStatus(transactionId, processor) {
        console.log("ExperienceHub - Get Available Bonusess");
        let instance = this;
        return new Promise(function (fulfill, reject) {
            fulfill();
            reject(instance.result("get_transaction_status_system_error"));
            /*let headers = new Headers({
             "Content-Type": "application/json",
             });
             fetch(instance.url+'transactionStatus/'+transactionId+(processor !== null ? '?processor='+processor : ''), {
             method: 'GET',
             headers: headers,
             credentials: 'same-origin'
             })
             .then(function (response){
             response.json().then(function (response) {
             if(!response.hasOwnProperty("info")){
             reject(instance.result("get_transaction_status_system_error"));
             return;
             }
             if(instance.handleErrors(response)){
             reject();
             }else{
             let success = response.info.success;
             let resultCode = response.info.resultCode;
             if(success){
             fulfill(response.data);
             }else{
             switch(resultCode){
             default:
             reject(instance.result("get_transaction_status_system_error"));
             break;
             }
             }
             }
             }).catch(function(){
             reject(instance.result("get_transaction_status_system_error"));
             });
             }).catch(function (error) {
             reject(instance.result("get_transaction_status_system_error"));
             });*/
        });

    }

    getAvailableBonuses() {
        console.log("ExperienceHub - Get Available Bonusess");
        let instance = this;
        return new Promise(function (fulfill, reject) {
            /*let headers = new Headers({
             "Content-Type": "application/json",
             });
             fetch(instance.url+'availableBonuses/'+instance.language, {
             method: 'GET',
             headers: headers,
             credentials: 'same-origin'
             })
             .then(function (response){
             response.json().then(function (response) {
             if(!response.hasOwnProperty("info")){
             reject(instance.result("get_available_bonuses_system_error"));
             return;
             }
             if(instance.handleErrors(response)){
             reject();
             }else{
             let success = response.info.success;
             let resultCode = response.info.resultCode;
             if(success){
             fulfill(response.data);
             }else{
             switch(resultCode){
             default:
             reject(instance.result("get_available_bonuses_system_error"));
             break;
             }
             }
             }
             }).catch(function(){
             reject(instance.result("get_available_bonuses_system_error"));
             });
             }).catch(function (error) {
             reject(instance.result("get_available_bonuses_system_error"));
             });*/
        });

    }

    activateBonus(bonus_id, bonus_code) {
        console.log("ExperienceHub - Activate Bonusess");
        let instance = this;
        return new Promise(function (fulfill, reject) {
            fulfill();
            reject(instance.result("activate_bonus_system_error"));
            /*	let headers = new Headers({
             "Content-Type": "application/json",
             });
             fetch(instance.url+'activateBonus', {
             method: 'POST',
             headers: headers,
             body: JSON.stringify({
             bonus_id: bonus_id,
             bonus_code: bonus_code
             }),
             credentials: 'same-origin'
             })
             .then(function (response){
             response.json().then(function (response) {
             if(!response.hasOwnProperty("info")){
             reject(instance.result("activate_bonus_system_error"));
             return;
             }
             if(instance.handleErrors(response)){
             reject();
             }else{
             let success = response.info.success;
             let resultCode = response.info.resultCode;
             if(success){
             fulfill(response.data);
             }else{
             switch(resultCode){
             case "bonus_unavailable":
             reject(instance.result("activate_bonus_bonus_unavailable"));
             break;
             default:
             reject(instance.result("activate_bonus_system_error"));
             break;
             }
             }
             }
             }).catch(function(){
             reject(instance.result("activate_bonus_system_error"));
             });
             }).catch(function (error) {
             reject(instance.result("activate_bonus_system_error"));
             });*/
        });

    }

    validateBonus(bonus_code) {
        console.log("ExperienceHub - Validate Bonusess");
        let instance = this;
        return new Promise(function (fulfill, reject) {
            fulfill();
            reject(instance.result("validate_bonus_system_error"));
            /*let headers = new Headers({
             "Content-Type": "application/json",
             });
             fetch(instance.url+'validateBonus', {
             method: 'POST',
             headers: headers,
             body: JSON.stringify({
             bonus_code: bonus_code
             }),
             credentials: 'same-origin'
             })
             .then(function (response){
             response.json().then(function (response) {
             if(!response.hasOwnProperty("info")){
             reject(instance.result("validate_bonus_system_error"));
             return;
             }
             if(instance.handleErrors(response)){
             reject();
             }else{
             let success = response.info.success;
             let resultCode = response.info.resultCode;
             if(success){
             fulfill(response.data);
             }else{
             switch(resultCode){
             default:
             reject(instance.result("validate_bonus_system_error"));
             break;
             }
             }
             }
             }).catch(function(){
             reject(instance.result("validate_bonus_system_error"));
             });
             }).catch(function (error) {
             reject(instance.result("validate_bonus_system_error"));
             });*/
        });

    }

    deactivateBonus() {
        console.log("ExperienceHub - Deactivate Bonus");
        let instance = this;
        return new Promise(function (fulfill, reject) {
            fulfill();
            reject(instance.result("deactivate_bonus_system_error"));
            /*let headers = new Headers({
             "Content-Type": "application/json",
             });
             fetch(instance.url+'deactivateBonus', {
             method: 'GET',
             headers: headers,
             credentials: 'same-origin'
             })
             .then(function (response){
             response.json().then(function (response) {
             if(!response.hasOwnProperty("info")){
             reject(instance.result("deactivate_bonus_system_error"));
             return;
             }
             if(instance.handleErrors(response)){
             reject();
             }else{
             let success = response.info.success;
             let resultCode = response.info.resultCode;
             if(success){
             fulfill();
             }else{
             switch(resultCode){
             default:
             reject(instance.result("deactivate_bonus_system_error"));
             break;
             }
             }
             }
             }).catch(function(){
             reject(instance.result("deactivate_bonus_system_error"));
             });
             }).catch(function (error) {
             reject(instance.result("deactivate_bonus_system_error"));
             });*/
        });

    }

    getLimits() {
        console.log("ExperienceHub - Get Limits");
        let instance = this;
        return new Promise(function (fulfill, reject) {
            fulfill(response.data);
            reject(instance.result("get_limits_system_error"));
            /*		let headers = new Headers({
             "Content-Type": "application/json",
             });
             fetch(instance.url+'getLimits', {
             method: 'GET',
             headers: headers,
             credentials: 'same-origin'
             })
             .then(function (response){
             response.json().then(function (response) {
             if(!response.hasOwnProperty("info")){
             reject(instance.result("get_limits_system_error"));
             return;
             }
             if(instance.handleErrors(response)){
             reject();
             }else{
             let success = response.info.success;
             let resultCode = response.info.resultCode;
             if(success){
             fulfill(response.data);
             }else{
             switch(resultCode){
             default:
             reject(instance.result("get_limits_system_error"));
             break;
             }
             }
             }
             }).catch(function(){
             reject(instance.result("get_limits_system_error"));
             });
             }).catch(function (error) {
             reject(instance.result("get_limits_system_error"));
             });*/
        });

    }

    getLimit(type) {
        console.log("ExperienceHub - Get Limit");
        let instance = this;
        return new Promise(function (fulfill, reject) {
            fulfill(/*response.data*/);
            reject(instance.result("get_limit_system_error"));
            /*let headers = new Headers({
             "Content-Type": "application/json",
             });
             fetch(instance.url+'getLimit/'+type, {
             method: 'GET',
             headers: headers,
             credentials: 'same-origin'
             })
             .then(function (response){
             response.json().then(function (response) {
             if(!response.hasOwnProperty("info")){
             reject(instance.result("get_limit_system_error"));
             return;
             }
             if(instance.handleErrors(response)){
             reject();
             }else{
             let success = response.info.success;
             let resultCode = response.info.resultCode;
             if(success){
             fulfill(response.data);
             }else{
             switch(resultCode){
             default:
             reject(instance.result("get_limit_system_error"));
             break;
             }
             }
             }
             }).catch(function(){
             reject(instance.result("get_limit_system_error"));
             });
             }).catch(function (error) {
             reject(instance.result("get_limit_system_error"));
             });*/
        });

    }

    setLimit(type, password, amount, interval) {
        console.log("ExperienceHub - Set Limit");
        let instance = this;
        return new Promise(function (fulfill, reject) {
            fulfill();
            reject(instance.result("set_limit_system_error"));
            /*let headers = new Headers({
             "Content-Type": "application/json",
             });
             fetch(instance.url+'setLimit/'+type, {
             method: 'POST',
             headers: headers,
             body: JSON.stringify({
             password: password,
             amount: amount,
             interval: interval
             }),
             credentials: 'same-origin'
             })
             .then(function (response){
             response.json().then(function (response) {
             if(!response.hasOwnProperty("info")){
             reject(instance.result("set_limit_system_error"));
             return;
             }
             if(instance.handleErrors(response)){
             reject();
             }else{
             let success = response.info.success;
             let resultCode = response.info.resultCode;
             if(success){
             fulfill();
             }else{
             switch(resultCode){
             case "restriction_error_increase_pending":
             reject(instance.result("set_limit_restriction_error_increase_pending"));
             break;
             case "missing_or_invalid_password":
             reject(instance.result("set_limit_missing_or_invalid_password"));
             break;
             default:
             reject(instance.result("set_limit_system_error"));
             break;
             }
             }
             }
             }).catch(function(){
             reject(instance.result("set_limit_system_error"));
             });
             }).catch(function (error) {
             reject(instance.result("set_limit_system_error"));
             });*/
        });

    }

    freezeAccount(password, amount) {
        console.log("ExperienceHub - Freeze Account");
        let instance = this;
        return new Promise(function (fulfill, reject) {
            fulfill();
            reject(instance.result("freeze_account_system_error"));
            /*		let headers = new Headers({
             "Content-Type": "application/json",
             });
             fetch(instance.url+'freezeAccount', {
             method: 'POST',
             headers: headers,
             body: JSON.stringify({
             password: password,
             amount: amount
             }),
             credentials: 'same-origin'
             })
             .then(function (response){
             response.json().then(function (response) {
             if(!response.hasOwnProperty("info")){
             reject(instance.result("freeze_account_system_error"));
             return;
             }
             if(instance.handleErrors(response)){
             reject();
             }else{
             let success = response.info.success;
             let resultCode = response.info.resultCode;
             if(success){
             fulfill();
             }else{
             switch(resultCode){
             case "missing_or_invalid_password":
             reject(instance.result("freeze_account_missing_or_invalid_password"));
             break;
             default:
             reject(instance.result("freeze_account_system_error"));
             break;
             }
             }
             }
             }).catch(function(){
             reject(instance.result("freeze_account_system_error"));
             });
             }).catch(function (error) {
             reject(instance.result("freeze_account_system_error"));
             });*/
        });
    }

    deactivateAccount(password) {
        console.log("ExperienceHub - Deactivate Account");
        let instance = this;
        return new Promise(function (fulfill, reject) {
            fulfill();
            reject(instance.result("deactivate_account_system_error"));
            /*let headers = new Headers({
             "Content-Type": "application/json",
             });
             fetch(instance.url+'deactivateAccount', {
             method: 'POST',
             headers: headers,
             body: JSON.stringify({
             password: password
             }),
             credentials: 'same-origin'
             })
             .then(function (response){
             response.json().then(function (response) {
             if(!response.hasOwnProperty("info")){
             reject(instance.result("deactivate_account_system_error"));
             return;
             }
             if(instance.handleErrors(response)){
             reject();
             }else{
             let success = response.info.success;
             let resultCode = response.info.resultCode;
             if(success){
             fulfill();
             }else{
             switch(resultCode){
             case "missing_or_invalid_password":
             reject(instance.result("deactivate_account_missing_or_invalid_password"));
             break;
             default:
             reject(instance.result("deactivate_account_system_error"));
             break;
             }
             }
             }
             }).catch(function(){
             reject(instance.result("deactivate_account_system_error"));
             });
             }).catch(function (error) {
             reject(instance.result("deactivate_account_system_error"));
             });*/
        });

    }

    getTransactions(from, to) {
        console.log("ExperienceHub - Get Transactions");
        let instance = this;
        return new Promise(function (fulfill, reject) {
            fulfill(/*response.data*/);
            reject(instance.result("get_transactions_system_error"));
            /*let headers = new Headers({
             "Content-Type": "application/json",
             });
             fetch(instance.url+'transactions', {
             method: 'POST',
             headers: headers,
             body: JSON.stringify({
             from: from,
             to: to
             }),
             credentials: 'same-origin'
             })
             .then(function (response){
             response.json().then(function (response) {
             if(!response.hasOwnProperty("info")){
             reject(instance.result("get_transactions_system_error"));
             return;
             }
             if(instance.handleErrors(response)){
             reject();
             }else{
             let success = response.info.success;
             let resultCode = response.info.resultCode;
             if(success){
             fulfill(response.data);
             }else{
             switch(resultCode){
             default:
             reject(instance.result("get_transactions_system_error"));
             break;
             }
             }
             }
             }).catch(function(){
             reject(instance.result("get_transactions_system_error"));
             });
             }).catch(function (error) {
             reject(instance.result("get_transactions_system_error"));
             });*/
        });

    }

    registrationForm(schema) {
        console.log("ExperienceHub - Registration Form");
        let instance = this;
        return new Promise(function (fulfill, reject) {
            fulfill(/*response.data*/);
            reject(instance.result("registration_form_system_error"));
            /*let headers = new Headers({
             "Content-Type": "application/json",
             });
             fetch(instance.url+'registrationForm', {
             method: 'POST',
             headers: headers,
             body: JSON.stringify({
             schema: schema
             }),
             credentials: 'same-origin'
             })
             .then(function (response){
             response.json().then(function (response) {
             if(!response.hasOwnProperty("info")){
             reject(instance.result("registration_form_system_error"));
             return;
             }
             if(instance.handleErrors(response)){
             reject();
             }else{
             let success = response.info.success;
             let resultCode = response.info.resultCode;
             if(success){
             fulfill(response.data);
             }else{
             switch(resultCode){
             default:
             reject(instance.result("registration_form_system_error"));
             break;
             }
             }
             }
             }).catch(function(){
             reject(instance.result("registration_form_system_error"));
             });
             }).catch(function (error) {
             reject(instance.result("registration_form_system_error"));
             });*/
        });

    }

    submitRegistration(info, fields) {
        console.log("ExperienceHub - Submit Registration");
        let instance = this;
        return new Promise(function (fulfill, reject) {
            fulfill(/*response.data*/);
            reject(instance.result("submit_registration_system_error"));
            /*if(!info.termsChecked){
             reject(instance.result("submit_registration_must_check_tou"));
             return;
             }
             let headers = new Headers({
             "Content-Type": "application/json",
             });
             fetch(instance.url+'submitRegistration', {
             method: 'POST',
             headers: headers,
             body: JSON.stringify({
             info: info,
             fields: fields
             }),
             credentials: 'same-origin'
             })
             .then(function (response){
             response.json().then(function (response) {
             if(!response.hasOwnProperty("info")){
             reject(instance.result("submit_registration_system_error"));
             return;
             }
             if(instance.handleErrors(response)){
             reject();
             }else{
             let success = response.info.success;
             let resultCode = response.info.resultCode;
             if(success){
             if(response.data.autologin){
             instance.syncPlayer();
             }
             fulfill(response.data);
             }else{
             switch(resultCode){
             case "player_data_not_valid":
             reject(instance.result("submit_registration_player_data_not_valid", response.data));
             break;
             case "invalid_input":
             reject(instance.result("submit_registration_invalid_input", response.data));
             break;
             default:
             reject(instance.result("submit_registration_system_error"));
             break;
             }
             }
             }
             }).catch(function(){
             reject(instance.result("submit_registration_system_error"));
             });
             }).catch(function (error) {
             reject(instance.result("submit_registration_system_error"));
             });*/
        });

    }

    playerProfileForm() {
        console.log("ExperienceHub - Registration Form");
        let instance = this;
        return new Promise(function (fulfill, reject) {
            fulfill(/*response.data*/);
            reject(instance.result("player_profile_form_system_error"));
            /*let headers = new Headers({
             "Content-Type": "application/json",
             });
             fetch(instance.url+'playerProfileForm', {
             method: 'GET',
             headers: headers,
             credentials: 'same-origin'
             })
             .then(function (response){
             response.json().then(function (response) {
             if(!response.hasOwnProperty("info")){
             reject(instance.result("player_profile_form_system_error"));
             return;
             }
             if(instance.handleErrors(response)){
             reject();
             }else{
             let success = response.info.success;
             let resultCode = response.info.resultCode;
             if(success){
             fulfill(response.data);
             }else{
             switch(resultCode){
             default:
             reject(instance.result("player_profile_form_system_error"));
             break;
             }
             }
             }
             }).catch(function(){
             reject(instance.result("player_profile_form_system_error"));
             });
             }).catch(function (error) {
             reject(instance.result("player_profile_form_system_error"));
             });*/
        });

    }

    getPlayer() {
        console.log("ExperienceHub - Get Player");
        let instance = this;
        return new Promise(function (fulfill, reject) {
            fulfill(/*details*/);
            reject(instance.result("get_player_system_error"));
            /*	let headers = new Headers({
             "Content-Type": "application/json",
             });
             fetch(instance.url+'getPlayer', {
             method: 'GET',
             headers: headers,
             credentials: 'same-origin'
             })
             .then(function (response){
             response.json().then(function (response) {
             if(!response.hasOwnProperty("info")){
             reject(instance.result("get_player_system_error"));
             return;
             }
             if(instance.handleErrors(response)){
             reject();
             }else{
             let success = response.info.success;
             let resultCode = response.info.resultCode;
             if(success){
             let fields = response.data.fields;
             let details = {};
             for(let i = 0; i < fields.length; i++){
             switch (fields[i].dataType){
             case "string":
             details[fields[i].code] = fields[i].valueStr;
             break;
             case "number":
             details[fields[i].code] = fields[i].valueNum;
             break;
             case "boolean":
             details[fields[i].code] = fields[i].valueBool;
             break;
             case "array":
             details[fields[i].code] = fields[i].valueArr;
             break;
             case "object":
             details[fields[i].code] = fields[i].valueObj;
             break;
             default:
             details[fields[i].code] = null;
             break;
             }
             }
             instance.setPlayerDetails(details);
             sessionStorage.setItem('playerDetails', JSON.stringify({expires: Date.now() + (1000*instance.playerDataDuration), data: details}));
             fulfill(details);
             }else{
             switch(resultCode){
             default:
             reject(instance.result("get_player_system_error"));
             break;
             }
             }
             }
             }).catch(function(error){
             reject(instance.result("get_player_system_error"));
             });
             }).catch(function (error) {
             reject(instance.result("get_player_system_error"));
             });*/
        });

    }

    playerData() {
        let instance = this;
        return new Promise(function (fulfill, reject) {
            fulfill(/*player*/);
            reject(instance.result("player_data_system_error"));
            /*let playerDataStore = sessionStorage.getItem('playerDetails');
             if(playerDataStore === null){
             instance.getPlayer().then(function(player){
             fulfill(player);
             },function(error){
             reject(error);
             });
             }else{
             let playerData = JSON.parse(playerDataStore);
             if(playerData.expires > Date.now()){
             fulfill(playerData.data);
             }else{
             instance.getPlayer().then(function(player){
             fulfill(player);
             },function(error){
             reject(error);
             });
             }
             }*/
        });

    }

    updatePlayerProfile(info, fields) {
        console.log("ExperienceHub - Update Player Profile");
        let instance = this;
        return new Promise(function (fulfill, reject) {
            fulfill(/*response.data*/);
            reject(instance.result("update_profile_system_error"));
            /*let headers = new Headers({
             "Content-Type": "application/json",
             });
             fetch(instance.url+'updatePlayerProfile', {
             method: 'POST',
             headers: headers,
             body: JSON.stringify({
             info: info,
             fields: fields
             }),
             credentials: 'same-origin'
             })
             .then(function (response){
             response.json().then(function (response) {
             if(!response.hasOwnProperty("info")){
             reject(instance.result("update_profile_system_error"));
             return;
             }
             if(instance.handleErrors(response)){
             reject();
             }else{
             let success = response.info.success;
             let resultCode = response.info.resultCode;
             if(success){
             fulfill(response.data);
             }else{
             switch(resultCode){
             case "invalid_input":
             reject(instance.result("update_profile_invalid_input", response.data));
             break;
             case "player_data_not_valid":
             reject(instance.result("update_profile_player_data_not_valid", response.data));
             break;
             default:
             reject(instance.result("update_profile_system_error"));
             break;
             }
             }
             }
             }).catch(function(){
             reject(instance.result("update_profile_system_error"));
             });
             }).catch(function (error) {
             reject(instance.result("update_profile_system_error"));
             });*/
        });

    }

    forgotPassword(email) {
        console.log("ExperienceHub - Forgot Password");
        let instance = this;
        return new Promise(function (fulfill, reject) {
            fulfill();
            reject(instance.result("forgot_password_system_error"));
            /*let headers = new Headers({
             "Content-Type": "application/json",
             });
             fetch(instance.url+'forgotPassword', {
             method: 'POST',
             headers: headers,
             body: JSON.stringify({
             email: email
             }),
             credentials: 'same-origin'
             })
             .then(function (response){
             response.json().then(function (response) {
             if(!response.hasOwnProperty("info")){
             reject(instance.result("forgot_password_system_error"));
             return;
             }
             if(instance.handleErrors(response)){
             reject();
             }else{
             let success = response.info.success;
             let resultCode = response.info.resultCode;
             if(success){
             fulfill();
             }else{
             switch(resultCode){
             case "missing_email":
             reject(instance.result("forgot_password_missing_email"));
             break;
             case "player_not_found":
             reject(instance.result("forgot_password_player_not_found"));
             case "account_blocked":
             reject(instance.result("forgot_password_account_blocked"));
             break;
             default:
             reject(instance.result("forgot_password_system_error"));
             break;
             }
             }
             }
             }).catch(function(error){
             reject(instance.result("forgot_password_system_error"));
             });
             }).catch(function (error) {
             reject(instance.result("forgot_password_system_error"));
             });*/
        });

    }

    resetPassword(token, password) {
        console.log("ExperienceHub - Reset Password");
        let instance = this;
        return new Promise(function (fulfill, reject) {
            fulfill();
            reject(instance.result("reset_password_system_error"));
            /*let headers = new Headers({
             "Content-Type": "application/json",
             });
             fetch(instance.url+'resetPassword', {
             method: 'POST',
             headers: headers,
             body: JSON.stringify({
             token: token,
             password: password
             }),
             credentials: 'same-origin'
             })
             .then(function (response){
             response.json().then(function (response) {
             if(!response.hasOwnProperty("info")){
             reject(instance.result("reset_password_system_error"));
             return;
             }
             if(instance.handleErrors(response)){
             reject();
             }else{
             let success = response.info.success;
             let resultCode = response.info.resultCode;
             if(success){
             fulfill();
             }else{
             switch(resultCode){
             case "invalid_input":
             reject(instance.result("reset_password_invalid_input"), response.data);
             break;
             case "token_expired":
             reject(instance.result("reset_password_token_expired"));
             break;
             case "missing_password":
             reject(instance.result("reset_password_missing_password"));
             break;
             default:
             reject(instance.result("reset_password_system_error"));
             break;
             }
             }
             }
             }).catch(function(error){
             reject(instance.result("reset_password_system_error"));
             });
             }).catch(function (error) {
             reject(instance.result("reset_password_system_error"));
             });*/
        });

    }

    changePassword(old_password, new_password, new_password_confirm) {
        console.log("ExperienceHub - Change Password");
        let instance = this;
        return new Promise(function (fulfill, reject) {
            fulfill();
            reject(instance.result("change_password_system_error"));
            /*if(new_password != new_password_confirm){
             console.log(new_password_confirm);
             reject(instance.result("change_password_new_password_mismatch"));
             return;
             }
             let headers = new Headers({
             "Content-Type": "application/json",
             });
             fetch(instance.url+'changePassword', {
             method: 'POST',
             headers: headers,
             body: JSON.stringify({
             old_password: old_password,
             new_password: new_password
             }),
             credentials: 'same-origin'
             })
             .then(function (response){
             response.json().then(function (response) {
             if(!response.hasOwnProperty("info")){
             reject(instance.result("change_password_system_error"));
             return;
             }
             if(instance.handleErrors(response)){
             reject();
             }else{
             let success = response.info.success;
             let resultCode = response.info.resultCode;
             if(success){
             fulfill();
             }else{
             switch(resultCode){
             case "old_password_missing":
             reject(instance.result("change_password_old_password_missing"));
             break;
             case "new_password_missing":
             reject(instance.result("change_password_new_password_missing"));
             break;
             case "invalid_old_password":
             reject(instance.result("change_password_invalid_old_password"));
             break;
             case "invalid_new_password":
             reject(instance.result("change_password_invalid_new_password"));
             break;
             default:
             reject(instance.result("change_password_system_error"));
             break;
             }
             }
             }
             }).catch(function(error){
             reject(instance.result("change_password_system_error"));
             });
             }).catch(function (error) {
             reject(instance.result("change_password_system_error"));
             });*/
        });

    }

    cashier() {
        console.log("ExperienceHub - Cashier");
        let instance = this;
        return new Promise(function (fulfill, reject) {
            fulfill(/*response.data*/);
            reject(instance.result("cashier_system_error"));
            /*fetch(instance.url+'cashier', {
             method: 'GET',
             credentials: 'same-origin'
             })
             .then(function (response){
             response.json().then(function (response) {
             if(!response.hasOwnProperty("info")){
             reject(instance.result("cashier_system_error"));
             return;
             }
             if(instance.handleErrors(response)){
             reject();
             }else{
             let success = response.info.success;
             let resultCode = response.info.resultCode;
             if(success){
             fulfill(response.data);
             }else{
             switch(resultCode){
             default:
             reject(instance.result("cashier_system_error"));
             break;
             }
             }
             }
             }).catch(function(error){
             reject(instance.result("cashier_system_error"));
             });
             }).catch(function (error) {
             reject(instance.result("cashier_system_error"));
             });*/
        });

    }

    getGameLink(gamecode, funplay) {
        console.log("ExperienceHub - Forgot Password");
        let instance = this;
        return new Promise(function (fulfill, reject) {
            fulfill(/*response.data*/);
            reject(instance.result("get_game_link_system_error"));
            /*let headers = new Headers({
             "Content-Type": "application/json",
             });
             fetch(instance.url+'getGameLink', {
             method: 'POST',
             headers: headers,
             body: JSON.stringify({
             gamecode: gamecode,
             funplay: funplay
             }),
             credentials: 'same-origin'
             })
             .then(function (response){
             response.json().then(function (response) {
             if(!response.hasOwnProperty("info")){
             reject(instance.result("get_game_link_system_error"));
             return;
             }
             if(instance.handleErrors(response)){
             reject();
             }else{
             let success = response.info.success;
             let resultCode = response.info.resultCode;
             if(success){
             fulfill(response.data);
             }else{
             switch(resultCode){
             case "game_not_available":
             reject(instance.result("get_game_link_game_not_available"));
             break;
             case "game_not_found":
             reject(instance.result("get_game_link_game_not_found"));
             break;
             default:
             reject(instance.result("get_game_link_system_error"));
             break;
             }
             }
             }
             }).catch(function(error){
             reject(instance.result("get_game_link_system_error"));
             });
             }).catch(function (error) {
             reject(instance.result("get_game_link_system_error"));
             });*/
        });

    }

    getTournaments() {
        console.log("ExperienceHub - Get Tournaments");
        let instance = this;
        return new Promise(function (fulfill, reject) {
            fulfill(/*response.data*/);
            reject(instance.result("get_tournaments_system_error"));
            /*let headers = new Headers({
             "Content-Type": "application/json",
             });
             fetch(instance.url+'getTournaments', {
             method: 'GET',
             headers: headers,
             credentials: 'same-origin'
             })
             .then(function (response){
             response.json().then(function (response) {
             if(!response.hasOwnProperty("info")){
             reject(instance.result("get_tournaments_system_error"));
             return;
             }
             if(instance.handleErrors(response)){
             reject();
             }else{
             let success = response.info.success;
             let resultCode = response.info.resultCode;
             if(success){
             fulfill(response.data);
             }else{
             switch(resultCode){
             case "game_not_available":
             reject(instance.result("get_tournaments_game_not_available"));
             break;
             case "game_not_found":
             reject(instance.result("get_tournaments_game_not_found"));
             break;
             default:
             reject(instance.result("get_tournaments_system_error"));
             break;
             }
             }
             }
             }).catch(function(error){
             reject(instance.result("get_tournaments_system_error"));
             });
             }).catch(function (error) {
             reject(instance.result("get_tournaments_system_error"));
             });*/
        });

    }

    status() {
        console.log("ExperienceHub - Status");
        let instance = this;
        return new Promise(function (fulfill, reject) {
            fulfill(/*response.data.sessionValid*/);
            reject(instance.result("status_system_error"));
            /*let headers = new Headers({
             "Content-Type": "application/json",
             });
             fetch(instance.url+'status', {
             method: 'GET',
             headers: headers,
             credentials: 'same-origin'
             })
             .then(function (response){
             response.json().then(function (response) {
             if(!response.hasOwnProperty("info")){
             reject(instance.result("status_system_error"));
             return;
             }
             if(instance.handleErrors(response)){
             reject();
             }else{
             let success = response.info.success;
             let resultCode = response.info.resultCode;
             if(success){
             if(response.data.sessionValid != instance.auth){
             if(response.data.sessionValid){
             instance.setAuth(true);
             }else{
             instance.unsetPlayer();
             instance.setAuth(false);
             }
             }
             instance.auth = response.data.sessionValid;
             if(response.data.sessionValid && !response.data.touValid){
             instance.getTermsOfUse().then(function(response){
             instance.handlers.touInvalid(response);
             },function(error){
             console.log(error);
             })
             }
             if(response.data.performRealityCheck){
             instance.getRealityCheck().then(function(response){
             instance.handlers.realityCheck(response);
             }, function(error){
             console.log(error);
             })
             }
             fulfill(response.data.sessionValid);
             }else{
             switch(resultCode){
             default:
             reject(instance.result("status_system_error"));
             break;
             }
             }
             }
             }).catch(function(){
             reject(instance.result("status_system_error"));
             });
             }).catch(function (error) {
             reject(instance.result("status_system_error"));
             });*/
        });

    }

    balance() {
        console.log("ExperienceHub - Balance");
        let instance = this;
        return new Promise(function (fulfill, reject) {
            fulfill(response.data);
            reject(instance.result("balance_system_error"));
            /*let headers = new Headers({
             "Content-Type": "application/json",
             });
             fetch(instance.url+'balance', {
             method: 'GET',
             headers: headers,
             credentials: 'same-origin'
             })
             .then(function (response){
             response.json().then(function (response) {
             if(!response.hasOwnProperty("info")){
             reject(instance.result("balance_system_error"));
             return;
             }
             if(instance.handleErrors(response)){
             reject();
             }else{
             let success = response.info.success;
             let resultCode = response.info.resultCode;
             if(success){
             instance.setPlayerBalance(response.data);
             fulfill(response.data);
             }else{
             switch(resultCode){
             default:
             reject(instance.result("balance_system_error"));
             break;
             }
             }
             }
             }).catch(function(){
             reject(instance.result("balance_system_error"));
             });
             }).catch(function (error) {
             reject(instance.result("balance_system_error"));
             });*/
        });

    }

    getTermsOfUse() {
        console.log("ExperienceHub - Get Terms Of Use");
        let instance = this;
        return new Promise(function (fulfill, reject) {
            fulfill(/*response.data*/);
            reject(instance.result("get_terms_system_error"));
            /*let headers = new Headers({
             "Content-Type": "application/json",
             });
             fetch(instance.url+'getTermsOfUse/'+instance.language, {
             method: 'GET',
             headers: headers,
             credentials: 'same-origin'
             })
             .then(function (response){
             response.json().then(function (response) {
             if(!response.hasOwnProperty("info")){
             reject(instance.result("get_terms_system_error"));
             return;
             }
             if(instance.handleErrors(response)){
             reject();
             }else{
             let success = response.info.success;
             let resultCode = response.info.resultCode;
             if(success){
             fulfill(response.data);
             }else{
             switch(resultCode){
             default:
             reject(instance.result("get_terms_system_error"));
             break;
             }
             }
             }
             }).catch(function(){
             reject(instance.result("get_terms_system_error"));
             });
             }).catch(function (error) {
             reject(instance.result("get_terms_system_error"));
             });*/
        });

    }

    acceptTermsOfUse(version) {
        console.log("ExperienceHub - Get Terms Of Use");
        let instance = this;
        return new Promise(function (fulfill, reject) {
            fulfill(/*response.data*/);
            reject(instance.result("accept_terms_system_error"));
            /*let headers = new Headers({
             "Content-Type": "application/json",
             });
             fetch(instance.url+'acceptTermsOfUse', {
             method: 'POST',
             headers: headers,
             body: JSON.stringify({
             version: version
             }),
             credentials: 'same-origin'
             })
             .then(function (response){
             response.json().then(function (response) {
             if(!response.hasOwnProperty("info")){
             reject(instance.result("accept_terms_system_error"));
             return;
             }
             if(instance.handleErrors(response)){
             reject();
             }else{
             let success = response.info.success;
             let resultCode = response.info.resultCode;
             if(success){
             fulfill(response.data);
             }else{
             switch(resultCode){
             default:
             reject(instance.result("accept_terms_system_error"));
             break;
             }
             }
             }
             }).catch(function(){
             reject(instance.result("accept_terms_system_error"));
             });
             }).catch(function (error) {
             reject(instance.result("accept_terms_system_error"));
             });*/
        });

    }

    getRealityCheck() {
        console.log("ExperienceHub - Get Reality Check");
        let instance = this;
        return new Promise(function (fulfill, reject) {
            fulfill(/*response.data*/);
            reject(instance.result("get_reality_check_system_error"));
            /*let headers = new Headers({
             "Content-Type": "application/json",
             });
             fetch(instance.url+'getReality', {
             method: 'GET',
             headers: headers,
             credentials: 'same-origin'
             })
             .then(function (response){
             response.json().then(function (response) {
             if(!response.hasOwnProperty("info")){
             reject(instance.result("get_reality_check_system_error"));
             return;
             }
             if(instance.handleErrors(response)){
             reject();
             }else{
             let success = response.info.success;
             let resultCode = response.info.resultCode;
             if(success){
             fulfill(response.data);
             }else{
             switch(resultCode){
             default:
             reject(instance.result("get_reality_check_system_error"));
             break;
             }
             }
             }
             }).catch(function(){
             reject(instance.result("get_reality_check_system_error"));
             });
             }).catch(function (error) {
             reject(instance.result("get_reality_check_system_error"));
             });*/
        });

    }

    confirmRealityCheck() {
        console.log("ExperienceHub - Get Reality Check");
        let instance = this;
        return new Promise(function (fulfill, reject) {
            fulfill();
            reject(instance.result("confirm_reality_check_system_error"));
            /*let headers = new Headers({
             "Content-Type": "application/json",
             });
             fetch(instance.url+'confirmReality', {
             method: 'GET',
             headers: headers,
             credentials: 'same-origin'
             })
             .then(function (response){
             response.json().then(function (response) {
             if(!response.hasOwnProperty("info")){
             reject(instance.result("confirm_reality_check_system_error"));
             return;
             }
             if(instance.handleErrors(response)){
             reject();
             }else{
             let success = response.info.success;
             let resultCode = response.info.resultCode;
             if(success){
             fulfill();
             }else{
             switch(resultCode){
             default:
             reject(instance.result("confirm_reality_check_system_error"));
             break;
             }
             }
             }
             }).catch(function(){
             reject(instance.result("confirm_reality_check_system_error"));
             });
             }).catch(function (error) {
             reject(instance.result("confirm_reality_check_system_error"));
             });*/
        });

    }

    syncPlayer() {

    }
}


module.exports = ExpHubApi;